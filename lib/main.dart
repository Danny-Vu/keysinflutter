import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Widget> tiles = [];

  @override
  void initState() {
    super.initState();
    tiles =  [
      Padding(
        key: UniqueKey(),
        padding: const EdgeInsets.all(8.0),
        child: const StatefulColorfulTile(),
      ),
      Padding(
        key: UniqueKey(),
        padding: const EdgeInsets.all(8.0),
        child: const StatefulColorfulTile(),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child:
        Row(children: tiles,),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.sentiment_very_satisfied),
        onPressed: swapTiles,
      ),
    );
  }
  
  void swapTiles() {
    setState(() {
      tiles.insert(1, tiles.removeAt(0));
    });
  }
}

class StatelessColorfulTile extends StatelessWidget {

   StatelessColorfulTile({super.key,});

  Color? color = UniqueColorGenerator.getColor();

    @override
  StatelessElement createElement() {
    // TODO: implement createElement
     // color = UniqueColorGenerator.getColor();
    return super.createElement();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: color,
        child: const Padding(
          padding: EdgeInsets.all(70.0),
        )
    );
  }
}

class UniqueColorGenerator {
  static Random random =  Random();

  static Color getColor() {
    return Color.fromARGB(255, random.nextInt(255), random.nextInt(255), random.nextInt(255));
  }
}


class StatefulColorfulTile extends StatefulWidget {

  const StatefulColorfulTile({super.key,});

  @override
  State<StatefulColorfulTile> createState() => _StatefulColorfulTileState();
}

class _StatefulColorfulTileState extends State<StatefulColorfulTile> {
  Color? color = UniqueColorGenerator.getColor();


  @override
  Widget build(BuildContext context) {
    return Container(
        color: color,
        child: const Padding(
          padding: EdgeInsets.all(70.0),
        )
    );
  }
}


